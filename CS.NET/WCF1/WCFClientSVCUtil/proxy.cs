﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.261
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


using System;

[System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
[System.ServiceModel.ServiceContractAttribute(ConfigurationName="IStockService")]
public interface IStockService
{
    
    [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IStockService/GetPrice", ReplyAction="http://tempuri.org/IStockService/GetPriceResponse")]
    double GetPrice(string _text);
}

[System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
public interface IStockServiceChannel : IStockService, System.ServiceModel.IClientChannel
{
}

[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
public partial class StockServiceClient : System.ServiceModel.ClientBase<IStockService>, IStockService
{
    
    public StockServiceClient()
    {
    }
    
    public StockServiceClient(string endpointConfigurationName) : 
            base(endpointConfigurationName)
    {
    }
    
    public StockServiceClient(string endpointConfigurationName, string remoteAddress) : 
            base(endpointConfigurationName, remoteAddress)
    {
    }
    
    public StockServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
            base(endpointConfigurationName, remoteAddress)
    {
    }
    
    public StockServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
            base(binding, remoteAddress)
    {
    }
    
    public double GetPrice(string _text)
    {
        return base.Channel.GetPrice(_text);
    }

    static void Main(string[] args)
    {
        // clasa definita in proxy.cs si creata de utilitarul svcutil.exe
        StockServiceClient proxy = new StockServiceClient();
        // apel metoda din serviciu
        double dd = proxy.GetPrice("Iasi");
        Console.WriteLine("Client start...");
        Console.WriteLine("Valoare obtinuta din GetPrice = {0}", dd);
        Console.ReadLine();
    }
}
